MOZILLA_INCLUDE=/usr/include/mozilla
#MOZILLA_INCLUDE=/home/blizzard/src/mozilla/mozilla/dist/include
GNOME_CFLAGS:=$(shell gnome-config --cflags gnomeui)
GNOME_LDFLAGS:=$(shell gnome-config --libs gnomeui)

CFLAGS= -I$(MOZILLA_INCLUDE) \
	-I$(MOZILLA_INCLUDE)/nspr \
	-I$(MOZILLA_INCLUDE)/gtkembedmoz \
	-I$(MOZILLA_INCLUDE)/widget \
	-I$(MOZILLA_INCLUDE)/gfx \
	-I$(MOZILLA_INCLUDE)/xpcom \
	-I$(MOZILLA_INCLUDE)/string \
	-I$(MOZILLA_INCLUDE)/webbrwsr \
	-I$(MOZILLA_INCLUDE)/content \
	-I$(MOZILLA_INCLUDE)/layout \
	-I$(MOZILLA_INCLUDE)/dom \
	$(GNOME_CFLAGS) \
	-Wall -g

LDFLAGS=-lpthread $(GNOME_LDFLAGS) \
	-lxpcom -lplds4 -lplc4 -lnspr4 -lgtkembedmoz -lgtksuperwin

all: prezilla.cpp
	gcc $(CFLAGS) prezilla.cpp -o prezilla-bin $(LDFLAGS)

clean:
	rm -f *.o *~ prezilla-bin
