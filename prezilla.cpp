#include <gnome.h>
#include <stdio.h>
#include <errno.h>

#include <gtkmozembed.h>
#include <gtkmozembed_internal.h>
#include <gtkmozarea.h>

#include <nsIWidget.h>
#include <nsCOMPtr.h>
#include <nsIDOMWindow.h>
#include <nsIDOMWindowInternal.h>
#include <nsPIDOMWindow.h>
#include <nsIWebBrowser.h>
#include <nsVoidArray.h>
#include <nsString.h>
#include <nsIDOMKeyEvent.h>
#include <nsReadableUtils.h>

nsVoidArray pageList;

PRBool      isRunning = PR_FALSE;
int         curSlide = 0;

void     printUsage(char *aProgName);
nsresult loadPageList(char *aFileName);
void     createControlWindow(void);
void     createRenderWindows(void);

void     loadSlide(int aSlideNumber);
void     nextSlide(void);
void     prevSlide(void);
void     endSlide(void);
void     cacheSlides(void);
void     showCurSlide(void);

gint
dom_key_press_cb     (GtkMozEmbed *embed, nsIDOMKeyEvent *event,
		      gpointer data);
void
focus_in_event_cb    (GtkMozArea *area,
		      GtkMozEmbed *embed);
void
focus_out_event_cb   (GtkMozArea *area,
		      GtkMozEmbed *embed);

GtkWidget *embed_prev          = NULL;
GtkWidget *embed_cur           = NULL;
GtkWidget *embed_next          = NULL;

GtkWidget *window_prev         = NULL;
GtkWidget *window_cur          = NULL;
GtkWidget *window_next         = NULL;

int main(int argc, char **argv) {
  if (argc < 2 || argv[1] == NULL) {
    printUsage(argv[0]);
    return 1;
  }

  nsresult rv;
  rv = loadPageList(argv[1]);
  if (NS_FAILED(rv)) {
    fprintf(stderr, "Failed to load page list: %s\n", strerror(errno));
  }

  gnome_init ("Prezilla", "0.1", argc, argv);

  createRenderWindows();

  createControlWindow();

  return 0;
}

void
printUsage(char *aProgName)
{
  fprintf(stderr, "Usage: %s config_file\n", aProgName);
}

nsresult
loadPageList(char *aFileName)
{
  FILE *file;
  char buf[256];
  size_t read = 0;
  PRInt32 found = 0;

  nsCString contents;
  nsCString tempString;

  file = fopen(aFileName, "r");
  if (!file)
    return NS_ERROR_FAILURE;

  // Read the file into memory.  Shouldn't be too big..
  do {
    read = fread(buf, sizeof(char), 256, file);
    if (read > 0) {
      contents.Append(buf, read);
    }
  } while (read != 0);

  // parse it.
  do {
    found = contents.Find("\n", PR_FALSE);
    // is the first char a \n?
    if (found == 0) {
      contents.Cut(0, 1);
      continue;
    }
    if (found > 0) {
      contents.Mid(tempString, 0, found);
      contents.Cut(0, found + 1);
      pageList.AppendElement(ToNewCString(tempString));
    }
    tempString.Truncate();
  } while (found > -1);
  
  fclose(file);
  return NS_OK;

}

void
dialog_clicked (GtkWidget *dialog, gint button, gpointer data)
{
  switch (button)
    {
    case 0:
      g_print ("Actually load up the file\n");
      return;
    case 1:
      if (isRunning)
	return;
      isRunning = PR_TRUE;
      loadSlide(0);
      return;
    default:
      gtk_main_quit ();
    }
}

void
createControlWindow(void)
{
  GtkWidget *dialog;
  GtkWidget *hbox;
  GtkWidget *scrolled_window;
  GtkWidget *clist;
  gint i;

  dialog = gnome_dialog_new ("Prezilla!!!", NULL);
  gtk_widget_set_usize (dialog, 500, 240);
  gtk_window_set_policy (GTK_WINDOW (dialog), FALSE, TRUE, FALSE);
  gnome_dialog_append_button (GNOME_DIALOG (dialog), GNOME_STOCK_PIXMAP_OPEN);
  gnome_dialog_append_button_with_pixmap (GNOME_DIALOG (dialog), "Run", GNOME_STOCK_PIXMAP_FORWARD);
  gnome_dialog_append_button (GNOME_DIALOG (dialog), GNOME_STOCK_PIXMAP_CLOSE);
  hbox = gtk_hbox_new (FALSE, 8);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  clist = gtk_clist_new (1);
  for (i = 0; i < pageList.Count(); i++) {
       gchar *row[1];
       gint index;

       row[0] = (char *) pageList[i];
       index = gtk_clist_append (GTK_CLIST (clist), row);
       gtk_clist_set_selectable (GTK_CLIST (clist), index, FALSE);
  }
  gtk_clist_columns_autosize (GTK_CLIST (clist));
  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hbox, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), scrolled_window, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (scrolled_window), clist);

  gnome_dialog_close_hides (GNOME_DIALOG (dialog), FALSE);
  gtk_signal_connect (GTK_OBJECT (dialog), "delete_event", GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  gtk_signal_connect (GTK_OBJECT (dialog), "clicked", GTK_SIGNAL_FUNC (dialog_clicked), NULL);
  gtk_widget_show_all (dialog);
  gtk_main ();
}

void
createRenderWindows(void)
{

  gint screen_width;
  gint screen_height;

  screen_width = gdk_screen_width();
  screen_height = gdk_screen_height();

  window_prev = gtk_window_new(GTK_WINDOW_POPUP);
  window_cur = gtk_window_new(GTK_WINDOW_POPUP);
  window_next = gtk_window_new(GTK_WINDOW_POPUP);

  /*
  window_prev = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  window_cur = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  window_next = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  */

  /*  gtk_widget_set_usize(window_prev, screen_width/2, screen_height/2);
  gtk_widget_set_usize(window_cur, screen_width/2, screen_height/2);
  gtk_widget_set_usize(window_next, screen_width/2, screen_height/2);
  */

  gtk_widget_set_usize(window_prev, screen_width, screen_height);
  gtk_widget_set_usize(window_cur, screen_width, screen_height);
  gtk_widget_set_usize(window_next, screen_width, screen_height);

  embed_prev = gtk_moz_embed_new();
  embed_cur = gtk_moz_embed_new();
  embed_next = gtk_moz_embed_new();

  gtk_container_add(GTK_CONTAINER(window_prev), embed_prev);
  gtk_container_add(GTK_CONTAINER(window_cur), embed_cur);
  gtk_container_add(GTK_CONTAINER(window_next), embed_next);

  gtk_widget_realize(window_prev);
  gtk_widget_realize(embed_prev);
  gtk_widget_realize(window_cur);
  gtk_widget_realize(embed_cur);
  gtk_widget_realize(window_next);
  gtk_widget_realize(embed_next);

  gtk_window_set_focus(GTK_WINDOW(window_prev), GTK_BIN(embed_prev)->child);
  gtk_window_set_focus(GTK_WINDOW(window_cur), GTK_BIN(embed_cur)->child);
  gtk_window_set_focus(GTK_WINDOW(window_next), GTK_BIN(embed_next)->child);

  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_prev)->child),
		     "toplevel_focus_in",
		     GTK_SIGNAL_FUNC(focus_in_event_cb), embed_prev);
  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_prev)->child),
		     "toplevel_focus_out",
		     GTK_SIGNAL_FUNC(focus_out_event_cb), embed_prev);

  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_cur)->child),
		     "toplevel_focus_in",
		     GTK_SIGNAL_FUNC(focus_in_event_cb), embed_cur);
  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_cur)->child),
		     "toplevel_focus_out",
		     GTK_SIGNAL_FUNC(focus_out_event_cb), embed_cur);

  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_next)->child),
		     "toplevel_focus_in",
		     GTK_SIGNAL_FUNC(focus_in_event_cb), embed_next);
  gtk_signal_connect(GTK_OBJECT(GTK_BIN(embed_next)->child),
		     "toplevel_focus_out",
		     GTK_SIGNAL_FUNC(focus_out_event_cb), embed_next);

  gtk_signal_connect(GTK_OBJECT(embed_prev), "dom_key_press",
		     GTK_SIGNAL_FUNC(dom_key_press_cb), NULL);
  gtk_signal_connect(GTK_OBJECT(embed_cur), "dom_key_press",
		     GTK_SIGNAL_FUNC(dom_key_press_cb), NULL);
  gtk_signal_connect(GTK_OBJECT(embed_next), "dom_key_press",
		     GTK_SIGNAL_FUNC(dom_key_press_cb), NULL);

}

gint
dom_key_press_cb     (GtkMozEmbed *embed, nsIDOMKeyEvent *event,
		      gpointer data)
{
  PRUint32 keyCode = 0;
  event->GetKeyCode(&keyCode);

  fprintf (stderr, "got key press\n");
  if (keyCode == nsIDOMKeyEvent::DOM_VK_RIGHT)
    nextSlide();
  if (keyCode == nsIDOMKeyEvent::DOM_VK_LEFT)
    prevSlide();
  if (keyCode == nsIDOMKeyEvent::DOM_VK_ESCAPE || 
      keyCode == nsIDOMKeyEvent::DOM_VK_Q)
    endSlide();
  return 1;
}

void
focus_in_event_cb    (GtkMozArea *area,
		      GtkMozEmbed *embed)
{

  // HACK HACK HACK FUCK HACK

  // focus the internal DOM window so that the right window will be in
  // focus when the focus event actually occurs.

  nsCOMPtr<nsIWebBrowser> browser;
  gtk_moz_embed_get_nsIWebBrowser(embed, getter_AddRefs(browser));

  nsCOMPtr<nsIDOMWindow> domWindow;
  browser->GetContentDOMWindow(getter_AddRefs(domWindow));

  nsCOMPtr<nsIDOMWindowInternal> domWindowInternal;
  domWindowInternal = do_QueryInterface(domWindow);

  // and make the toplevel think that it just got a focus in

  GdkEvent event;
  event.type = GDK_FOCUS_CHANGE;
  event.focus_change.window = 0;
  event.focus_change.send_event = FALSE;
  event.focus_change.in = TRUE;
  gtk_widget_event(GTK_WIDGET(gtk_widget_get_toplevel(GTK_WIDGET(area))),
		   &event);

  // and focus the internal widget

  domWindowInternal->Focus();

}

void
focus_out_event_cb   (GtkMozArea *area,
		      GtkMozEmbed *embed)
{
  nsCOMPtr<nsIWebBrowser> browser;
  gtk_moz_embed_get_nsIWebBrowser(embed, getter_AddRefs(browser));

  nsCOMPtr<nsIDOMWindow> domWindow;
  browser->GetContentDOMWindow(getter_AddRefs(domWindow));

  nsCOMPtr<nsIDOMWindowInternal> domWindowInternal;
  domWindowInternal = do_QueryInterface(domWindow);

  nsCOMPtr<nsPIDOMWindow> piDOMWindow;
  piDOMWindow = do_QueryInterface(domWindowInternal);

  piDOMWindow->Deactivate();

  // make the toplevel think it just got a focus out

  GdkEvent event;
  event.type = GDK_FOCUS_CHANGE;
  event.focus_change.window = 0;
  event.focus_change.send_event = FALSE;
  event.focus_change.in = FALSE;
  gtk_widget_event(GTK_WIDGET(gtk_widget_get_toplevel(GTK_WIDGET(area))),
		   &event);
}

void
loadSlide(int aSlideNumber)
{
  const char *curPageURL;

  if (aSlideNumber + 1 > pageList.Count()) {
    fprintf(stderr, "invalid slide offset %d\n", aSlideNumber);
    return;
  }

  curSlide = aSlideNumber;

  curPageURL = (const char *)pageList.ElementAt(curSlide);

  gtk_moz_embed_load_url(GTK_MOZ_EMBED(GTK_BIN(window_cur)->child),
			 curPageURL);

  showCurSlide();
  
  cacheSlides();
}

void
nextSlide(void)
{
  if (curSlide + 1 >= pageList.Count()) {
    fprintf(stderr, "tried to advance past end of slides\n");
    return;
  }

  gtk_grab_remove(window_cur);

  curSlide++;

  GtkWidget *tmp;
  tmp = window_prev;
  window_prev = window_cur;
  window_cur = window_next;
  window_next = tmp;

  showCurSlide();

  cacheSlides();

}

void
prevSlide(void)
{
  if (curSlide == 0) {
    fprintf(stderr, "tried to move past the beginning of slides\n");
    return;
  }

  gtk_grab_remove(window_cur);

  curSlide--;
  
  GtkWidget *tmp;
  tmp = window_next;
  window_next = window_cur;
  window_cur = window_prev;
  window_prev = tmp;

  showCurSlide();

  cacheSlides();
}

void
endSlide(void)
{
  gtk_widget_hide(window_cur);
  gtk_grab_remove(window_cur);
  isRunning = PR_FALSE;
}

void
cacheSlides(void)
{
  const char *next_url;
  const char *prev_url;
  printf("curSlide is %d count is %d\n", curSlide, pageList.Count());
  printf("prev: %p\ncur : %p\nnext: %p\n",
	 window_next, window_cur, window_prev);
  // are we at the beginning?
  if (pageList.Count() > curSlide + 1) {
    next_url = (const char *)pageList.ElementAt(curSlide + 1);
    printf("loading %s(%d) onto next\n", next_url, curSlide + 1);
    gtk_moz_embed_load_url(GTK_MOZ_EMBED(GTK_BIN(window_next)->child),
			   next_url);
  }
  if (curSlide > 0) {
    prev_url = (const char *)pageList.ElementAt(curSlide - 1);
    printf("loading %s(%d) onto prev\n", prev_url, curSlide - 1);
    gtk_moz_embed_load_url(GTK_MOZ_EMBED(GTK_BIN(window_prev)->child),
			   prev_url);
  }
}

void
showCurSlide(void)
{
  gtk_widget_show_all(window_cur);
  gtk_widget_hide(window_prev);
  gtk_widget_hide(window_next);

  gdk_keyboard_grab(window_cur->window, TRUE, GDK_CURRENT_TIME);
  gtk_grab_add(window_cur);
}
